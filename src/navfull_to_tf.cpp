/*
* Copyright (c) 2016 Carnegie Mellon University, Author <basti@andrew.cmu.edu >
*
* For License information please see the LICENSE file in the root directory.
*
*/

/*
 * Translates navfull msg to tf and odometry using UTM
 */

#include <ros/ros.h>
#include <message_filters/subscriber.h>
#include <message_filters/time_synchronizer.h>
#include <nav_msgs/Odometry.h>
#include <tf/transform_broadcaster.h>
#include <microstrain_3dm_gx3_45/NavFull.h>
#include <proj_api.h>
#include <Eigen/Eigen>
#include <ca_common/math.h>


static ros::Publisher odom_pub;
std::string frame_id, child_frame_id, local_frame_id;
double rot_cov,fixed_alt,mag_heading;
projPJ srcTfn;
projPJ destTfn;
tf::Quaternion q_mag;

//void callback(const sensor_msgs::NavSatFixConstPtr& fix) {
void callback(const microstrain_3dm_gx3_45::NavFullConstPtr& nav) {
  static tf::TransformBroadcaster tfbr,tfbrl;
  static tf::Transform Transform,Transforml;

  if (!(nav->filter_state && 0x02)) { // valid is 0x02 or 0x03
    ROS_WARN("Nav filter no valid solution.");
    return;
  }

  double lon = nav->longitude * M_PI/180.0;//expected in degrees
  double lat = nav->latitude * M_PI/180.0;//expected in degrees

  int ec =pj_transform(srcTfn,destTfn,1,1,&lon,&lat,NULL);
  if(ec !=0)
    ROS_ERROR_STREAM("Error transforming coordinates: "<<ec);

  double easting =  lon;//output in meters
  double northing = lat;//output in meters

  if (odom_pub) {
    nav_msgs::Odometry odom;
    odom.header.stamp = nav->header.stamp;

    odom.header.frame_id = frame_id;
    odom.child_frame_id = child_frame_id;

    odom.pose.pose.position.x = northing;
    odom.pose.pose.position.y = easting;
    if (isnan(fixed_alt))
	      odom.pose.pose.position.z = -nav->altitude;
    else
 	      odom.pose.pose.position.z = -fixed_alt;
//    tf::Quaternion q;
//    tf::quaternionMsgToTF(nav->quaternion, q);
//    tf::quaternionTFToMsg(q_mag*q, odom.pose.pose.orientation);

    Eigen::Quaterniond q = CA::msgc(nav->quaternion);

    odom.pose.pose.orientation = CA::msgc(q);


    // Use ENU covariance to build XYZRPY covariance
    boost::array<double, 36> covariance = {{
	pow(nav->north_uncertainty,2), 0, 0, 0, 0, 0,
	0, pow(nav->east_uncertainty,2), 0, 0, 0, 0, 
	0, 0, pow(nav->down_uncertainty,2), 0, 0, 0, 
	0, 0, 0, pow(nav->roll_uncertainty,2), 0, 0,
	0, 0, 0, 0, pow(nav->pitch_uncertainty,2), 0,
	0, 0, 0, 0, 0, pow(nav->yaw_uncertainty,2)
      }};

    odom.pose.covariance = covariance;

    Eigen::Vector3d nedVelocity(nav->velocity_north, nav->velocity_east, nav->velocity_down);

//    Eigen::Vector3d bodyVelocity = q.inverse()*nedVelocity;
//    odom.twist.twist.linear = CA::msgc(bodyVelocity);

    odom.twist.twist.linear = CA::msgc(nedVelocity);


    // TODO Proper transform covariance matrix

    boost::array<double, 36> twist_covariance = {{
	pow(nav->velocity_north_uncertainty,2), 0, 0, 0, 0, 0,
	0, pow(nav->velocity_east_uncertainty,2), 0, 0, 0, 0, 
	0, 0, pow(nav->velocity_down_uncertainty,2), 0, 0, 0, 
	0, 0, 0, -1, 0, 0,
	0, 0, 0, 0, -1, 0,
	0, 0, 0, 0, 0, -1
      }};
    odom.twist.covariance = twist_covariance;

    odom_pub.publish(odom);
    
  }
}

int main (int argc, char **argv) {
  ros::init(argc, argv, "navfull_to_tf_node");
  ros::NodeHandle node;
  ros::NodeHandle priv_node("~");

  //Initialize projections
  std::string source_frame,dest_frame;
  priv_node.param<std::string>("source_frame",source_frame,"+proj=latlon +datum=WGS84");
  priv_node.param<std::string>("dest_frame",dest_frame,"+proj=utm +zone=17N +datum=WGS84");
  ROS_INFO_STREAM("Source frame: "<<source_frame<<" Dest frame: "<<dest_frame);

  srcTfn = pj_init_plus(source_frame.c_str());
  destTfn = pj_init_plus(dest_frame.c_str());
  
  priv_node.param<std::string>("frame_id", frame_id, "/ned_orig");
  priv_node.param<std::string>("local_frame_id", local_frame_id, "/ned_orig");
  priv_node.param<std::string>("child_frame_id", child_frame_id, "/microstrain");
  priv_node.param<double>("fixed_alt", fixed_alt, nan(""));
  priv_node.param<double>("mag_heading", mag_heading, 0.0);
  q_mag = tf::createQuaternionFromYaw(mag_heading);
  std::cout <<"mag heading is "<< mag_heading <<" q "<<q_mag<<std::endl;

  odom_pub = node.advertise<nav_msgs::Odometry>("odom", 10);

  ros::TransportHints ros_transport_hints = ros::TransportHints().udp().tcpNoDelay();


  ros::Subscriber nav_sub = node.subscribe("navfull", 10, callback, ros_transport_hints);

  ros::spin();
}

