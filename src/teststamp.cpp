/*
* Copyright (c) 2016 Carnegie Mellon University, Author <basti@andrew.cmu.edu >
*
* For License information please see the LICENSE file in the root directory.
*
*/

#include "ros/ros.h"
#include <microstrain_3dm_gx3_45/ImuFull.h>
#include <microstrain_3dm_gx3_45/GpsFull.h>
#include <microstrain_3dm_gx3_45/NavFull.h>
#include <algorithm>

microstrain_3dm_gx3_45::ImuFull last_imu_full;
microstrain_3dm_gx3_45::GpsFull last_gps_full;
microstrain_3dm_gx3_45::NavFull last_nav_full;

double imu_sum = 0.0;
double imu_min = 100.0;
double imu_max = -100.0;
int imu_count = 0;

double gps_sum = 0.0;
double gps_min = 100.0;
double gps_max = -100.0;
int gps_count = 0;

double nav_sum = 0.0;
double nav_min = 100.0;
double nav_max = -100.0;
int nav_count = 0;

void imuFullCallback(const microstrain_3dm_gx3_45::ImuFull::ConstPtr& msg)
{
  double dt = (msg->header.stamp - last_imu_full.header.stamp).toSec();
  double dt_gps = msg->gps_time_of_wk - last_imu_full.gps_time_of_wk;
  double dt_error = dt-dt_gps;
  if(dt > 1.0)
  {
    last_imu_full = *msg;
    return;
  }
  
  if(dt_error > 0.0001 || dt_error < -0.0001)
  {
    ROS_WARN("IMU dt difference! TS:%f GPS:%f (%f)",dt,dt_gps,dt_error);
  }
  
  imu_sum += dt;
  imu_count++;
  imu_min = std::min(imu_min,dt);
  imu_max = std::max(imu_max,dt);
  
  last_imu_full = *msg;
}
 
void gpsFullCallback(const microstrain_3dm_gx3_45::GpsFull::ConstPtr& msg)
{
  double dt = (msg->header.stamp - last_gps_full.header.stamp).toSec();
  double dt_gps = msg->gps_time_of_wk - last_gps_full.gps_time_of_wk;
  double dt_error = dt-dt_gps;
  if(dt > 1.0)
  {
    last_gps_full = *msg;
    return;
  }
  
  if(dt_error > 0.0001 || dt_error < -0.0001)
  {
    ROS_WARN("GPS dt difference! TS:%f GPS:%f (%f)",dt,dt_gps,dt_error);
  }

  gps_sum += dt;
  gps_count++;
  gps_min = std::min(gps_min,dt);
  gps_max = std::max(gps_max,dt);
  
  last_gps_full = *msg;
}

void navFullCallback(const microstrain_3dm_gx3_45::NavFull::ConstPtr& msg)
{
  double dt = (msg->header.stamp - last_nav_full.header.stamp).toSec();
  double dt_gps = msg->gps_time_of_wk - last_nav_full.gps_time_of_wk;
  double dt_error = dt-dt_gps;
  if(dt > 1.0)
  {
    last_nav_full = *msg;
    return;
  }
  
  if(dt_error > 0.0001 || dt_error < -0.0001)
  {
    ROS_WARN("NAV dt difference! TS:%f GPS:%f (%f)",dt,dt_gps,dt_error);
  }

  nav_sum += dt;
  nav_count++;
  nav_min = std::min(nav_min,dt);
  nav_max = std::max(nav_max,dt);
  
  last_nav_full = *msg;
}

void timerCallback(const ros::TimerEvent& e)
{
  ROS_INFO("IMU - Min:%f Max:%f Average: %f",imu_min,imu_max,imu_sum/imu_count);
  ROS_INFO("GPS - Min:%f Max:%f Average: %f",gps_min,gps_max,gps_sum/gps_count);
  ROS_INFO("NAV - Min:%f Max:%f Average: %f",nav_min,nav_max,nav_sum/nav_count);
  ROS_INFO(" ");
}
  
int main(int argc, char **argv)
{
  ros::init(argc, argv, "microstrain_teststamp");
  ros::NodeHandle nh;
 
  ros::Subscriber imu_full_sub;
  ros::Subscriber gps_full_sub;
  ros::Subscriber nav_full_sub;
  
  imu_full_sub = nh.subscribe("/microstrain/imu_full", 100, imuFullCallback);
  gps_full_sub = nh.subscribe("/microstrain/gps_full", 100, gpsFullCallback);
  nav_full_sub = nh.subscribe("/microstrain/nav_full", 100, navFullCallback);
  
  ros::Timer timer = nh.createTimer(ros::Duration(1.0), timerCallback);

  ros::spin();

  return 0;
}
