/*
* Copyright (c) 2016 Carnegie Mellon University, Author <basti@andrew.cmu.edu >
*
* For License information please see the LICENSE file in the root directory.
*
*/

#include "ros/ros.h"
#include "sensor_msgs/Imu.h"
#include <microstrain_3dm_gx3_45/ImuFull.h>
#include <queue>

class MicrostrainRestamper {

public:
  MicrostrainRestamper(ros::NodeHandle nh, ros::NodeHandle np);
  ~MicrostrainRestamper();
  void imuCallback(const sensor_msgs::Imu::ConstPtr& msg);
  void imuFullCallback(const microstrain_3dm_gx3_45::ImuFull::ConstPtr& msg);
  void processQueue( void );

private:
  
  ros::NodeHandle nh_, np_;
  ros::Publisher imu_pub_;
  ros::Subscriber imu_sub_;
  ros::Subscriber imu_full_sub_;

  std::queue<sensor_msgs::Imu> imuMsgs;
  std::queue<microstrain_3dm_gx3_45::ImuFull> fullMsgs;
  

};
 
  // Constructor 
  MicrostrainRestamper::MicrostrainRestamper(ros::NodeHandle nh, ros::NodeHandle np):
    nh_(nh),
    np_(np)
  {
    imu_sub_ = nh_.subscribe("/microstrain/imu", 100, &MicrostrainRestamper::imuCallback, this);
    imu_full_sub_ = nh_.subscribe("/microstrain/imu_full", 100, &MicrostrainRestamper::imuFullCallback, this);

    imu_pub_ = nh_.advertise<sensor_msgs::Imu>("/microstrain/imu_restamped", 100);
    
    //imuMsgs.clear();
    //fullMsgs.clear();
  }
  
  MicrostrainRestamper::~MicrostrainRestamper()
  {
    //imuMsgs.clear();
    //fullMsgs.clear();
  }


  void MicrostrainRestamper::imuCallback(const sensor_msgs::Imu::ConstPtr& msg)
  {
    imuMsgs.push(*msg); 
    processQueue();

  } 

  void MicrostrainRestamper::imuFullCallback(const microstrain_3dm_gx3_45::ImuFull::ConstPtr& msg)
  {
    fullMsgs.push(*msg);
    processQueue();
  }
  
  void MicrostrainRestamper::processQueue( void )
  {
    while (!imuMsgs.empty() && !fullMsgs.empty())
    {
      if (imuMsgs.front().header.seq == fullMsgs.front().header.seq)
      {
        // Create new imu message with correct stamp and publish it
        
        sensor_msgs::Imu restampedMsg = imuMsgs.front();
        restampedMsg.header = fullMsgs.front().header;
        // It seems that you cannot directly modify the seq number
        //restampedMsg.header.seq = fullMsgs.front().header.seq;
        imu_pub_.publish(restampedMsg);
        
        // Discard the used messages
        fullMsgs.pop();
        imuMsgs.pop();
      }
      else if (imuMsgs.front().header.seq > fullMsgs.front().header.seq)
      {
        // Discard the old full msg
        fullMsgs.pop();
      }
      else
      {
        // Discard the old imu msg
        imuMsgs.pop();
      }
    }
    
    
  }


int main(int argc, char **argv)
{
  ros::init(argc, argv, "microstrain_restamper");
  ros::NodeHandle nh;
  ros::NodeHandle np("~");

  MicrostrainRestamper restamper(nh, np);

  ros::spin();

  return 0;
}
