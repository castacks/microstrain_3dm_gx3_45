/*
* Copyright (c) 2016 Carnegie Mellon University, Author <achamber>
*
* For License information please see the LICENSE file in the root directory.
*
*/

/*
 * altitude_offset_calculator.cpp
 *
 *  Created on: Apr 23, 2013
 *      Author: achamber
 */

#include <ros/ros.h>
#include <microstrain_3dm_gx3_45/GpsFull.h>

void gpsfullCallback(const microstrain_3dm_gx3_45::GpsFull::ConstPtr& msg)
{
	double offset = msg->alt_ellipsoid - msg->alt_msl;
	std::cout << offset << std::endl;
	ros::param::set("/missiondata/dem/zoffset",  offset);
}


int main (int argc, char **argv) {
	ros::init(argc, argv, "msl_hae_offset");
	ros::NodeHandle node;
	ros::NodeHandle priv_node("~");

	ros::Subscriber gpsfull_sub = node.subscribe("gps_full", 10, gpsfullCallback);

	ros::spin();

	return 0;
}








