/*
* Copyright (c) 2016 Carnegie Mellon University, Author <basti@andrew.cmu.edu >
*
* For License information please see the LICENSE file in the root directory.
*
*/

/*
 * Copyright 2011 Virginia Polytechnic Institute and State University.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 		* Redistributions of source code must retain the above copyright
 * 		  notice, this list of conditions and the following disclaimer.
 * 		* Redistributions in binary form must reproduce the above copyright
 * 		  notice, this list of conditions and the following disclaimer in the
 * 		  documentation and/or other materials provided with the distribution.
 * 		* Neither the name of the <organization> nor the
 * 		  names of its contributors may be used to endorse or promote products
 * 		  derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 *
 * main.cpp
 *
 *  Created on: Apr 13, 2011
 *      Author: kevin

 * Edited by sebastian scherer, Justin Haines
 */


#include "ms3dmgx345.h"
#include <iostream>
#include <ros/ros.h>
#include <sensor_msgs/Imu.h>
#include <microstrain_3dm_gx3_45/ImuFull.h>
#include <microstrain_3dm_gx3_45/GpsFull.h>
#include <microstrain_3dm_gx3_45/NavFull.h>
#include <riverine_watchdog/watchdog.h>


using namespace std;

using namespace microstrain::gx345;

CA::PetWatchdog pet;

class MyHandler : public GX3Handler
{
public:
  static const int INITIAL_OFFSET_SAMPLE_COUNT = 200;
  static const int INITIAL_OFFSET_SAMPLE_COUNT_GPS = 10;
  ros::NodeHandle imu_node_handle;
  ros::Publisher imu_data_pub;
  ros::Publisher imu_full_pub;
  ros::Publisher gps_full_pub;
  ros::Publisher nav_full_pub;

  int receivedgps;
  int receivedgpsSent;
  int receivedimu;
  int receivedimuSent;
  int receivednav;
  int receivednavSent;

  double imuTimeOffset;
  int    imuTimeCount;
  double gpsTimeOffset;
  int    gpsTimeCount;
  double navTimeOffset;
  int    navTimeCount;

  //Buffers
  sensor_msgs::Imu imu_data;
  microstrain_3dm_gx3_45::ImuFull imu_full;
  microstrain_3dm_gx3_45::GpsFull gps_full;
  microstrain_3dm_gx3_45::NavFull nav_full;

  MyHandler():
    GX3Handler(),
    receivedgps(0),
    receivedgpsSent(0),
    receivedimu(0),
    receivedimuSent(0),
    receivednav(0),
    receivednavSent(0),
    imuTimeOffset(0),
    imuTimeCount(0),
    gpsTimeOffset(0),
    gpsTimeCount(0),
    navTimeOffset(0),
    navTimeCount(0)
  {
    imu_data_pub = imu_node_handle.advertise<sensor_msgs::Imu>("/microstrain/imu", 100);
    imu_full_pub = imu_node_handle.advertise<microstrain_3dm_gx3_45::ImuFull>("/microstrain/imu_full", 100);
    gps_full_pub = imu_node_handle.advertise<microstrain_3dm_gx3_45::GpsFull>("/microstrain/gps_full", 4);
    nav_full_pub = imu_node_handle.advertise<microstrain_3dm_gx3_45::NavFull>("/microstrain/nav_full", 100);
  }
  virtual geometry_msgs::Vector3 convert(const ahrs::_xyzfloats &a)
  {
    geometry_msgs::Vector3 res;
    res.x = a.x;
    res.y = a.y;
    res.z = a.z;
    return res;
  }
  virtual geometry_msgs::Vector3 convert(const nav::_xyzfloats &a)
  {
    geometry_msgs::Vector3 res;
    res.x = a.x;
    res.y = a.y;
    res.z = a.z;
    return res;
  }

  virtual void quaternion(const ahrs::quaternion::t &t)
  {
    receivedimu++;
    ROS_DEBUG_STREAM(" Quaternion: "<<t.q[0]<<" "<<t.q[1]<<" "<<t.q[2]<<" "<<t.q[3]);
    imu_data.orientation.x = t.q[1];
    imu_data.orientation.y = t.q[2];
    imu_data.orientation.z = t.q[3];
    imu_data.orientation.w = t.q[0];

  }
  virtual void scaled_accel(const ahrs::scaled_accel::t &d)
  {
    receivedimu++;
    ROS_DEBUG_STREAM("Scaled accel: " << d.x << ", " << d.y<< ", " << d.z);
    imu_data.linear_acceleration = convert(d);
    //Units in g convert to m/s^2
    imu_data.linear_acceleration.x *= 9.80665;
    imu_data.linear_acceleration.y *= 9.80665;
    imu_data.linear_acceleration.z *= 9.80665;

  }
  virtual void scaled_gyro(const ahrs::scaled_gyro::t &d)
  {
    receivedimu++;
    ROS_DEBUG_STREAM( "Scaled gyro: " << d.x << ", " << d.y << ", " << d.z);
    imu_data.angular_velocity = convert(d);
  }
  virtual void scaled_mag(const ahrs::scaled_mag::t &d)
  {
    receivedimu++;
    ROS_DEBUG_STREAM( "Magnetometer: " << d.x << ", " << d.y << ", " << d.z);
    imu_full.magnetometer = convert(d);

  }
  virtual void internal_time(const ahrs::internal_time::t &d)
  {
    receivedimu++;
    ROS_DEBUG_STREAM("Internal time: " << d);
    imu_full.internal_time = d;
    if(imuTimeCount<INITIAL_OFFSET_SAMPLE_COUNT)
    {
      double diff = ros::Time::now().toSec() - ((double)d/((double)62500.0));

	// When imuTimeOffset goes to infinity, diff will become meaningless - Andrew
      //imuTimeOffset = (imuTimeOffset * imuTimeCount + diff)/ (imuTimeCount+1);

	double alpha = 0.95;
      imuTimeOffset = imuTimeOffset * alpha + diff * (1-alpha);

      imuTimeCount++;
      if(imuTimeCount % 4 ==0)
        ROS_INFO_STREAM("IMU Time offset: "<<imuTimeOffset);
    }

  }
  virtual void beacon_time(const ahrs::beacon_time::t &d)
  {
    receivedimu++;
    ROS_DEBUG_STREAM("Beacon time ");
    imu_full.beacon_status = d.status;
    imu_full.beacon_pps_cnt = d.pps_cnt;
    imu_full.beacon_ns_cnt = d.ns_cnt;
  }
  virtual void gps_time(const ahrs::gps_time::t &d)
  {
    receivedimu++;
    ROS_DEBUG_STREAM("GPS time: " << d.time_of_wk << ", " << d.wk_num<< ", flags: " << d.valid_flags);
    imu_full.gps_time_of_wk = d.time_of_wk;
    imu_full.gps_week       = d.wk_num;
    imu_full.gps_valid      = d.valid_flags;
  }

  virtual void delta_theta(const ahrs::delta_theta::t &d)
  {
    receivedimu++;
    ROS_DEBUG_STREAM("Delta Theta: " << d.x << ", " << d.y << ", " << d.z);
    imu_full.delta_theta = convert(d);
  }
  virtual void delta_velocity(const ahrs::delta_velocity::t &d)
  {
    receivedimu++;
    ROS_DEBUG_STREAM("Delta Velocity: " << d.x << ", " << d.y<< ", " << d.z);
    imu_full.delta_velocity = convert(d);
  }

  virtual void pps_phase_shift(const ahrs::pps_phase_shift::t &d)
  {
    receivedimu++;
    ROS_DEBUG_STREAM(" PPS Phase Shift: " << d.phase << " ns Valid: " << d.valid_flags << " Timestamp: " << d.sw_stamp);
    imu_full.pps_phase_shift = d.phase;
    imu_full.sw_timestamp = d.sw_stamp;
    imu_full.pps_valid = d.valid_flags;
  }

  /* The gps data components */

  virtual void llh_position(const gps::llh_position::t &d)
  {
    receivedgps++;
    ROS_DEBUG_STREAM("LLH Position: " << d.latitude << ", " << d.longitude<< ", " << d.alt_ellipsoid << ", " << d.alt_msl << ", " << d.horizontal_acc << ", " << d.vertical_acc << ", " << d.valid_flags);
    gps_full.latitude       = d.latitude;
    gps_full.longitude      = d.longitude;
    gps_full.alt_ellipsoid  = d.alt_ellipsoid;
    gps_full.alt_msl        = d.alt_msl;
    gps_full.horizontal_acc = d.horizontal_acc;
    gps_full.vertical_acc   = d.vertical_acc;
    gps_full.llh_valid      = d.valid_flags;
  }

  virtual void ned_velocity(const gps::ned_velocity::t &d)
  {
    receivedgps++;
    ROS_DEBUG_STREAM(" ned vel ");
    gps_full.ned_velocity.x = d.north;
    gps_full.ned_velocity.y = d.east;
    gps_full.ned_velocity.z = d.down;
    gps_full.ned_speed      = d.speed;
    gps_full.ned_ground_spd = d.ground_spd;
    gps_full.ned_heading    = d.heading;
    gps_full.ned_spd_acc    = d.spd_acc;
    gps_full.ned_head_acc   = d.head_acc;
    gps_full.ned_valid      = d.valid_flags;
  }

  virtual void gps_time(const gps::gps_time::t &d)
  {
    receivedgps++;
    ROS_DEBUG_STREAM(" gps time ");
    gps_full.gps_time_of_wk = d.time_of_wk;
    gps_full.gps_week       = d.wk_num;
    gps_full.gps_valid      = d.valid_flags;

    if(gpsTimeCount<INITIAL_OFFSET_SAMPLE_COUNT_GPS)
    {
      double diff = ros::Time::now().toSec() - d.time_of_wk;
      gpsTimeOffset = (gpsTimeOffset * gpsTimeCount + diff)/ (gpsTimeCount+1);
      gpsTimeCount++;
      ROS_INFO_STREAM("GPS Time offset: "<<gpsTimeOffset);
    }
  }

  virtual void gps_fix(const gps::gps_fix::t &d)
  {
    receivedgps++;
    ROS_DEBUG_STREAM(" gps fix ");
    gps_full.fix_type     = d.fix_type;
    gps_full.fix_num_sats = d.num_sats;
    gps_full.fix_flags    = d.fix_flags;
    gps_full.fix_valid    = d.valid_flags;
  }

  virtual void dop_data(const gps::dop_data::t &d)
  {
    receivedgps++;
    ROS_DEBUG_STREAM("dop ");
    gps_full.dop_geom = d.geom;
    gps_full.dop_pos = d.pos;
    gps_full.dop_horiz = d.horiz;
    gps_full.dop_vert = d.vert;
    gps_full.dop_time = d.time;
    gps_full.dop_northing = d.northing;
    gps_full.dop_easting = d.easting;
    gps_full.dop_valid = d.valid_flags;
  }

  virtual void utc_time(const gps::utc_time::t &d)
  {
    receivedgps++;
    ROS_DEBUG_STREAM(" utc time ");
    gps_full.utc_year   = d.year;
    gps_full.utc_month  = d.month;
    gps_full.utc_hour   = d.hour;
    gps_full.utc_minute = d.minute;
    gps_full.utc_second = d.second;
    gps_full.utc_millis = d.millis;
    gps_full.utc_valid  = d.valid_flags;
  }

  virtual void clock_info(const gps::clock_info::t &d)
  {
    receivedgps++;
    ROS_DEBUG_STREAM(" clock info ");
    gps_full.clk_bias  = d.clk_bias;
    gps_full.clk_drift = d.clk_drift;
    gps_full.clk_acc   = d.acc;
    gps_full.clk_valid = d.valid_flags;
  }

  virtual void pps_phase_shift(const gps::pps_phase_shift::t &d)
  {
    receivedgps++;
    ROS_DEBUG_STREAM(" PPS Phase Shift: " << d.phase << " ns Valid: " << d.valid_flags << " Timestamp: " << d.sw_stamp);
    gps_full.pps_phase_shift = d.phase;
    gps_full.sw_timestamp = d.sw_stamp;
    gps_full.pps_valid = d.valid_flags;
  }

  /* The nav data components */

  virtual void filter_status(const nav::filter_status::t &d)
  {
	  receivednav++;
	  ROS_DEBUG_STREAM(" filter_status ");
	  nav_full.filter_state = d.filter_state;
	  nav_full.dynamics_mode = d.dynamics_mode;
	  nav_full.status_flags = d.status_flags;
  }

  virtual void gps_time(const nav::gps_time::t &d)
  {
	  receivednav++;
	  ROS_DEBUG_STREAM(" gps_time ");
	  nav_full.gps_time_of_wk = d.time_of_wk;
	  nav_full.gps_week = d.wk_num;
	  nav_full.gps_valid = d.valid_flags;

    if(navTimeCount<INITIAL_OFFSET_SAMPLE_COUNT)
    {
      double diff = ros::Time::now().toSec() - d.time_of_wk;
      navTimeOffset = (navTimeOffset * navTimeCount + diff)/ (navTimeCount+1);
      navTimeCount++;
      ROS_INFO_STREAM("NAV Time offset: "<<navTimeOffset);
    }
  }

  virtual void est_llh_position(const nav::est_llh_position::t &d)
  {
	  receivednav++;
	  ROS_DEBUG_STREAM(" est_llh_position ");
	  nav_full.latitude = d.latitude;
	  nav_full.longitude = d.longitude;
	  nav_full.altitude = d.alt_ellipsoid;
	  nav_full.llh_position_valid = d.valid_flags;
  }

  virtual void est_ned_velocity(const nav::est_ned_velocity::t &d)
  {
	  receivednav++;
	  ROS_DEBUG_STREAM(" est_ned_velocity ");
	  nav_full.velocity_north = d.north;
	  nav_full.velocity_east = d.east;
	  nav_full.velocity_down = d.down;
	  nav_full.ned_velocity_valid = d.valid_flags;
  }

  virtual void est_quaternion(const nav::est_quaternion::t &d)
  {
	  receivednav++;
	  ROS_DEBUG_STREAM(" est_quaternion ");
	  nav_full.quaternion.x = d.q[1];
	  nav_full.quaternion.y = d.q[2];
	  nav_full.quaternion.z = d.q[3];
	  nav_full.quaternion.w = d.q[0];
	  nav_full.quaternion_valid = d.valid_flags;
  }

  virtual void est_orien_mat(const nav::est_orien_mat::t &d)
  {
	  receivednav++;
	  ROS_DEBUG_STREAM(" est_orien_mat ");
	  for(int i=0; i<9; i++)
	    nav_full.orientation[i] = d.M[i];
  }

  virtual void est_euler_angles(const nav::est_euler_angles::t &d)
  {
	  receivednav++;
	  ROS_DEBUG_STREAM(" est_euler_angles ");
	  nav_full.roll = d.roll;
	  nav_full.pitch = d.pitch;
	  nav_full.yaw = d.yaw;
	  nav_full.euler_valid = d.valid_flags;
  }

  virtual void est_gyro_bias(const nav::est_gyro_bias::t &d)
  {
	  receivednav++;
	  ROS_DEBUG_STREAM(" est_gyro_bias ");
	  nav_full.gyro_bias = convert(d.d);
	  nav_full.gyro_bias_valid = d.valid_flags;
  }

  virtual void est_llh_position_uncertainty(const nav::est_llh_position_uncertainty::t &d)
  {
	  receivednav++;
	  ROS_DEBUG_STREAM(" est_llh_position_uncertainty ");
	  nav_full.north_uncertainty = d.north;
	  nav_full.east_uncertainty = d.east;
	  nav_full.down_uncertainty = d.down;
	  nav_full.llh_position_uncertainty_valid = d.valid_flags;
  }

  virtual void est_ned_velocity_uncertainty(const nav::est_ned_velocity_uncertainty::t &d)
  {
	  receivednav++;
	  ROS_DEBUG_STREAM(" est_ned_velocity_uncertainty ");
    nav_full.velocity_north_uncertainty = d.north;
    nav_full.velocity_east_uncertainty = d.east;
    nav_full.velocity_down_uncertainty = d.down;
    nav_full.ned_velocity_uncertainty_valid = d.valid_flags;
  }

  virtual void est_euler_angle_uncertainty(const nav::est_euler_angle_uncertainty::t &d)
  {
	  receivednav++;
	  ROS_DEBUG_STREAM(" est_euler_angle_uncertainty ");
	  nav_full.roll_uncertainty = d.roll;
	  nav_full.pitch_uncertainty = d.pitch;
	  nav_full.yaw_uncertainty = d.yaw;
	  nav_full.euler_uncertainty_valid = d.valid_flags;
  }

  virtual void est_gyro_bias_uncertainty(const nav::est_gyro_bias_uncertainty::t &d)
  {
	  receivednav++;
	  ROS_DEBUG_STREAM(" est_gyro_bias_uncertainty ");
	  nav_full.gyro_bias_uncertainty = convert(d.d);
	  nav_full.gyro_bias_uncertainty_valid = d.valid_flags;
  }

  virtual void est_lin_accel(const nav::est_lin_accel::t &d)
  {
	  receivednav++;
	  ROS_DEBUG_STREAM(" est_lin_accel ");
	  nav_full.linear_accel = convert(d.d);
	  nav_full.linear_accel_valid = d.valid_flags;
  }

  virtual void est_angular_rate(const nav::est_angular_rate::t &d)
  {
	  receivednav++;
	  ROS_DEBUG_STREAM(" est_angular_rate ");
	  nav_full.angular_rate = convert(d.d);
	  nav_full.angular_rate_valid = d.valid_flags;
  }

  virtual void gravity_magnitude(const nav::gravity_magnitude::t &d)
  {
	  receivednav++;
	  ROS_DEBUG_STREAM(" gravity_magnitude ");
	  nav_full.gravity_magnitude = d.g;
	  nav_full.gravity_magnitude_valid = d.valid_flags;
  }

  virtual void est_quaternion_uncertainty(const nav::est_quaternion_uncertainty::t &d)
  {
	  receivednav++;
	  ROS_DEBUG_STREAM(" est_quaternion_uncertainty ");
	  nav_full.quaternion_uncertainty[0] = d.q[0];
	  nav_full.quaternion_uncertainty[1] = d.q[1];
	  nav_full.quaternion_uncertainty[2] = d.q[2];
	  nav_full.quaternion_uncertainty[3] = d.q[3];
  }

  virtual void est_gravity_vector(const nav::est_gravity_vector::t &d)
  {
	  receivednav++;
	  ROS_DEBUG_STREAM(" est_gravity_vector ");
	  nav_full.gravity = convert(d.d);
	  nav_full.gravity_valid = d.valid_flags;
  }

  virtual void heading(const nav::heading::t &d)
  {
	  receivednav++;
	  ROS_DEBUG_STREAM(" heading ");
	  nav_full.heading = d.heading;
	  nav_full.heading_uncertainty = d.heading_sigma;
	  nav_full.heading_source = d.source;
	  nav_full.heading_valid = d.valid_flags;
  }

  virtual void magnetic_model(const nav::magnetic_model::t &d)
  {
	  receivednav++;
	  ROS_DEBUG_STREAM(" magnetic_model ");
	  nav_full.mag_north_intensity = d.north;
	  nav_full.mag_east_intensity = d.east;
	  nav_full.mag_down_intensity = d.down;
	  nav_full.mag_inclination = d.inclination;
	  nav_full.mag_declination = d.declination;
	  nav_full.mag_valid = d.valid_flags;
  }

  virtual void pps_phase_shift(const nav::pps_phase_shift::t &d)
  {
    receivednav++;
    ROS_DEBUG_STREAM(" PPS Phase Shift: " << d.phase << " ns Valid: " << d.valid_flags << " Timestamp: " << d.sw_stamp);
    nav_full.pps_phase_shift = d.phase;
    nav_full.sw_timestamp = d.sw_stamp;
    nav_full.pps_valid = d.valid_flags;
  }


  virtual void eop()
  {

    // Watchdog
		pet.alive();

    //
    // Its time to send a gps message
    //
    if(receivedgps != receivedgpsSent)
    {
      ROS_DEBUG_STREAM("EOP. Sending gps");
      receivedgpsSent = receivedgps;

      // If some sort of low-level stamping is available, use it
      if(gps_full.pps_valid != 0)
      {
        int nsecs;
        ros::Time now = ros::Time::now();
        // If we have hardware synchronization, use it
        if(gps_full.pps_valid & 0x2)
        {
          nsecs = ros::Time(gps_full.gps_time_of_wk).nsec + gps_full.pps_phase_shift;
          nsecs %= 1000000000;
        }
        // If no hw sync, low-level sw is next best
        else
        {
          nsecs = gps_full.sw_timestamp;
        }

        // If the clock rolled over, we need to stamp the message in the previous second
        if(now.nsec < 4e8 && nsecs > 6e8)
          gps_full.header.stamp.sec = now.sec - 1;
        else
          gps_full.header.stamp.sec = now.sec;

        gps_full.header.stamp.nsec = nsecs;
      }
      // Resort to internal timestamping
      else
      {
        if(gpsTimeCount>=INITIAL_OFFSET_SAMPLE_COUNT_GPS)
        {
          gps_full.header.stamp = ros::Time(gps_full.gps_time_of_wk+ gpsTimeOffset);
        }
        else
        {
          gps_full.header.stamp = ros::Time::now();
        }
      }
      gps_full.header.frame_id = "microstrain";
      gps_full_pub.publish(gps_full);
    }
    //
    // It's time to send an IMU message
    //
    if(receivedimu != receivedimuSent)
    {
      ROS_DEBUG_STREAM("EOP. Sending imu");
      receivedimuSent = receivedimu;

      // If some sort of low-level stamping is available, use it
      if(imu_full.pps_valid != 0)
      {
        int nsecs;
        ros::Time now = ros::Time::now();
        // If we have hardware synchronization, use it
        // TODO obviously this code won't run, wtf
        // commenting it out for now
        // basically it will take the else {} branch
        //if(imu_full.pps_valid & 0x0)
        //{
        //  nsecs = ros::Time(imu_full.gps_time_of_wk).nsec + imu_full.pps_phase_shift;
        //  nsecs %= 1000000000;
        //}
        // If no hw sync, low-level sw is next best
        //else
        //{
        nsecs = imu_full.sw_timestamp;
        //}

        // If the clock rolled over, we need to stamp the message in the previous second
        if(now.nsec < 4e8 && nsecs > 6e8)
          imu_full.header.stamp.sec = now.sec - 1;
        else
          imu_full.header.stamp.sec = now.sec;

        imu_full.header.stamp.nsec = nsecs;
      }
      // Resort to internal timestamping
      else
      {
        if(imuTimeCount>=INITIAL_OFFSET_SAMPLE_COUNT)
        {
          imu_full.header.stamp = ros::Time(((double)imu_full.internal_time/62500.0)+ imuTimeOffset);
        }
        else
        {
          imu_full.header.stamp = ros::Time::now();
        }
      }

      imu_full.header.frame_id = "microstrain";
      imu_data.header = imu_full.header;
      imu_data_pub.publish(imu_data);
      imu_full_pub.publish(imu_full);
    }

    //
    // It's time to send a nav message
    //
    if(receivednav != receivednavSent)
    {
      ROS_DEBUG_STREAM("EOP. Sending nav");
      receivednavSent = receivednav;

      // If some sort of low-level stamping is available, use it
      if(nav_full.pps_valid != 0)
      {
        int nsecs;
        ros::Time now = ros::Time::now();
        // If we have hardware synchronization, use it
        if(nav_full.pps_valid & 0x2)
        {
          nsecs = ros::Time(nav_full.gps_time_of_wk).nsec + nav_full.pps_phase_shift;
          nsecs %= 1000000000;
        }
        // If no hw sync, low-level sw is next best
        else
        {
          nsecs = nav_full.sw_timestamp;
        }

        // If the clock rolled over, we need to stamp the message in the previous second
        if(now.nsec < 4e8 && nsecs > 6e8)
          nav_full.header.stamp.sec = now.sec - 1;
        else
          nav_full.header.stamp.sec = now.sec;

        nav_full.header.stamp.nsec = nsecs;
      }
      // Resort to internal timestamping
      else
      {
        if(navTimeCount >= INITIAL_OFFSET_SAMPLE_COUNT)
        {
          nav_full.header.stamp = ros::Time(nav_full.gps_time_of_wk+ navTimeOffset);
        }
        else
        {
          nav_full.header.stamp = ros::Time::now();
        }
      }
      nav_full.header.frame_id = "microstrain";
      nav_full_pub.publish(nav_full);
    }
  }
};


int main(int argc, char **argv)
{
	//Now start ROS
	ros::init(argc, argv, "microstrain_3dm_gx3_45");
	ros::NodeHandle n;
	ros::NodeHandle np("~");

  if(!pet.setup(np))
  {
    ROS_ERROR_STREAM("microstrain_3dm_gx3_45: Was not able to setup watchdog");
    return -1;
  }

	std::string portname;
  double orientation_stdev;
  double angular_velocity_stdev;
  double linear_acceleration_stdev;

	if(!n.getParam("/microstrain_3dm_gx3_45/port",portname))
	  {
	    ROS_ERROR_STREAM("Port not defined");
	    return -1;
	  }

	if(!np.getParam("orientation_stdev",orientation_stdev))
	  {
	    ROS_ERROR_STREAM("Orientation standard deviation not defined");
	    return -1;
	  }

	if(!np.getParam("angular_velocity_stdev",angular_velocity_stdev))
	  {
	    ROS_ERROR_STREAM("Angular velocity standard deviation not defined");
	    return -1;
	  }

	if(!np.getParam("linear_acceleration_stdev",linear_acceleration_stdev))
	  {
	    ROS_ERROR_STREAM("Linear acceleration standard deviation not defined");
	    return -1;
	  }

	MyHandler h;
	SerialGX345 gx3(portname);
	gx3.setGX3Handler(&h);
	if (!gx3.isGood())
	{
	  ROS_ERROR_STREAM("port not opened!");
	  return -1;
	}
	ROS_INFO_STREAM("successfully opened port!");

	//Setup the messages we want to send
	h.imu_data.orientation_covariance[0] = pow(orientation_stdev,2);
  h.imu_data.orientation_covariance[1] = 0;
  h.imu_data.orientation_covariance[2] = 0;
  h.imu_data.orientation_covariance[3] = 0;
  h.imu_data.orientation_covariance[4] = pow(orientation_stdev,2);
  h.imu_data.orientation_covariance[5] = 0;
  h.imu_data.orientation_covariance[6] = 0;
  h.imu_data.orientation_covariance[7] = 0;
  h.imu_data.orientation_covariance[8] = pow(orientation_stdev,2);

  h.imu_data.angular_velocity_covariance[0] = pow(angular_velocity_stdev,2);
  h.imu_data.angular_velocity_covariance[1] = 0;
  h.imu_data.angular_velocity_covariance[2] = 0;
  h.imu_data.angular_velocity_covariance[3] = 0;
  h.imu_data.angular_velocity_covariance[4] = pow(angular_velocity_stdev,2);
  h.imu_data.angular_velocity_covariance[5] = 0;
  h.imu_data.angular_velocity_covariance[6] = 0;
  h.imu_data.angular_velocity_covariance[7] = 0;
  h.imu_data.angular_velocity_covariance[8] = pow(angular_velocity_stdev,2);

	h.imu_data.linear_acceleration_covariance[0] = pow(linear_acceleration_stdev,2);
  h.imu_data.linear_acceleration_covariance[1] = 0;
  h.imu_data.linear_acceleration_covariance[2] = 0;
  h.imu_data.linear_acceleration_covariance[3] = 0;
  h.imu_data.linear_acceleration_covariance[4] = pow(linear_acceleration_stdev,2);
  h.imu_data.linear_acceleration_covariance[5] = 0;
  h.imu_data.linear_acceleration_covariance[6] = 0;
  h.imu_data.linear_acceleration_covariance[7] = 0;
  h.imu_data.linear_acceleration_covariance[8] = pow(linear_acceleration_stdev,2);


	std::vector<char> ds,ds2,r,r2;
	ds.push_back( microstrain::gx345::ahrs::quaternion::desc);r.push_back(10);
	ds.push_back( microstrain::gx345::ahrs::scaled_accel::desc);r.push_back(10);
	ds.push_back( microstrain::gx345::ahrs::scaled_gyro::desc);r.push_back(10);
	ds.push_back( microstrain::gx345::ahrs::scaled_mag::desc);r.push_back(10);
	ds.push_back( microstrain::gx345::ahrs::internal_time::desc);r.push_back(10);
	ds.push_back( microstrain::gx345::ahrs::beacon_time::desc);r.push_back(10);
	ds.push_back( microstrain::gx345::ahrs::gps_time::desc);r.push_back(10);
	ds.push_back( microstrain::gx345::ahrs::delta_theta::desc);r.push_back(10);
	ds.push_back( microstrain::gx345::ahrs::delta_velocity::desc);r.push_back(10);
	// Don't setnd config packets for now:
	//gx3.set_ahrs(ds, r);
	ds2.push_back( microstrain::gx345::gps::llh_position::desc);r2.push_back(1);
	ds2.push_back( microstrain::gx345::gps::ned_velocity::desc);r2.push_back(1);
	ds2.push_back( microstrain::gx345::gps::gps_time::desc);r2.push_back(1);
	ds2.push_back( microstrain::gx345::gps::gps_fix::desc);r2.push_back(1);
	//ds2.push_back( microstrain::gx345::gps::dop_data::desc);r2.push_back(1);
	ds2.push_back( microstrain::gx345::gps::utc_time::desc);r2.push_back(1);
	//	ds2.push_back( microstrain::gx345::gps::clock_info::desc);r2.push_back(4);
	//gx3.set_gps(ds2, r2);

	//Spin and wait
	ros::spin();
	return 0;
}
