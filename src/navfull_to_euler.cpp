/*
* Copyright (c) 2016 Carnegie Mellon University, Author <basti@andrew.cmu.edu >
*
* For License information please see the LICENSE file in the root directory.
*
*/

#include "ros/ros.h"
#include <microstrain_3dm_gx3_45/ImuFull.h>
#include <microstrain_3dm_gx3_45/GpsFull.h>
#include <microstrain_3dm_gx3_45/NavFull.h>
#include <ca_common/math.h>
#include <geometry_msgs/Vector3Stamped.h>
#include <algorithm>

ros::Publisher euler_pub;

void imuFullCallback(const microstrain_3dm_gx3_45::ImuFull::ConstPtr& msg)
{

}
 
void gpsFullCallback(const microstrain_3dm_gx3_45::GpsFull::ConstPtr& msg)
{

}

void navFullCallback(const microstrain_3dm_gx3_45::NavFull::ConstPtr& msg)
{
  double x = msg->quaternion.x;
  double y = msg->quaternion.y;
  double z = msg->quaternion.z;
  double w = msg->quaternion.w;
  
  CA::QuatD quat(w,x,y,z);
  
  CA::Vector3D euler = CA::quatToEuler(quat);
  
  geometry_msgs::Vector3Stamped out;
  
  out.vector.x = euler[0];
  out.vector.y = euler[1];
  out.vector.z = euler[2];
  
  out.header = msg->header;
  
  euler_pub.publish(out);
}
  
int main(int argc, char **argv)
{
  ros::init(argc, argv, "microstrain_teststamp");
  ros::NodeHandle nh;
 
  ros::Subscriber imu_full_sub;
  ros::Subscriber gps_full_sub;
  ros::Subscriber nav_full_sub;
  
  imu_full_sub = nh.subscribe("/microstrain/imu_full", 100, imuFullCallback);
  gps_full_sub = nh.subscribe("/microstrain/gps_full", 100, gpsFullCallback);
  nav_full_sub = nh.subscribe("/microstrain/nav_full", 100, navFullCallback);

  euler_pub = nh.advertise<geometry_msgs::Vector3Stamped>("/microstrain/euler",100);

  ros::spin();

  return 0;
}
