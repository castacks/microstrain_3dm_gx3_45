/*
* Copyright (c) 2016 Carnegie Mellon University, Author <basti@andrew.cmu.edu>
*
* For License information please see the LICENSE file in the root directory.
*
*/


// Converts our custom microstrain_3dm_gx3_45/GpsFull message to sensor_msgs/NavSatFix so we can reuse utm conversion nodes

#include "ros/ros.h"
#include "sensor_msgs/NavSatFix.h"
#include "sensor_msgs/NavSatStatus.h"
#include <microstrain_3dm_gx3_45/GpsFull.h>
#include <limits>

class GpsFullToFix {

public:
  GpsFullToFix(ros::NodeHandle nh, ros::NodeHandle np);
  ~GpsFullToFix();
  void GpsFullCallback(const microstrain_3dm_gx3_45::GpsFull::ConstPtr& msg);

private:
  
  ros::NodeHandle nh_, np_;
  ros::Publisher gpsfix_pub_;
  ros::Subscriber GpsFull_sub_;

};
 
  // Constructor 
  GpsFullToFix::GpsFullToFix(ros::NodeHandle nh, ros::NodeHandle np):
    nh_(nh),
    np_(np)
  {
    ros::TransportHints hints = ros::TransportHints().udp().tcpNoDelay(); 
    GpsFull_sub_ = nh_.subscribe("input", 100, &GpsFullToFix::GpsFullCallback, this, hints);
    gpsfix_pub_ = nh_.advertise<sensor_msgs::NavSatFix>("output", 100);
    
  }
  
  GpsFullToFix::~GpsFullToFix()
  {
  }


  void GpsFullToFix::GpsFullCallback(const microstrain_3dm_gx3_45::GpsFull::ConstPtr& msg)
  {
    /*
    NavSatStatus options
    # Whether to output an augmented fix is determined by both the fix
    # type and the last time differential corrections were received.  A
    # fix is valid when status >= STATUS_FIX.

    int8 STATUS_NO_FIX =  -1        # unable to fix position
    int8 STATUS_FIX =      0        # unaugmented fix
    int8 STATUS_SBAS_FIX = 1        # with satellite-based augmentation
    int8 STATUS_GBAS_FIX = 2        # with ground-based augmentation
    */
    sensor_msgs::NavSatStatus status;
    if ((msg->llh_valid & 0x0001) && (msg->llh_valid & 0x0002))
      status.status = sensor_msgs::NavSatStatus::STATUS_FIX;
    else
      status.status = sensor_msgs::NavSatStatus::STATUS_NO_FIX;

    status.service = sensor_msgs::NavSatStatus::SERVICE_GPS;

    sensor_msgs::NavSatFix output;
    output.header = msg->header;
    output.status = status;
    
    output.latitude = msg->latitude;
    output.longitude = msg->longitude;
    output.altitude = msg->alt_ellipsoid;
    
    /*
    # Position covariance [m^2] defined relative to a tangential plane
    # through the reported position. The components are East, North, and
    # Up (ENU), in row-major order.
    */
    output.position_covariance[0] = (msg->llh_valid & 0x0008) ? pow(msg->horizontal_acc,2)  : -1;
    output.position_covariance[4] = (msg->llh_valid & 0x0008) ? pow(msg->horizontal_acc,2)  : -1;
    output.position_covariance[8] = (msg->llh_valid & 0x0010) ? pow(msg->vertical_acc,2)    : -1;
     
    /*
    Pose covariance types:
    uint8 COVARIANCE_TYPE_UNKNOWN = 0
    uint8 COVARIANCE_TYPE_APPROXIMATED = 1
    uint8 COVARIANCE_TYPE_DIAGONAL_KNOWN = 2
    uint8 COVARIANCE_TYPE_KNOWN = 3
    */
    output.position_covariance_type = sensor_msgs::NavSatFix::COVARIANCE_TYPE_DIAGONAL_KNOWN;

  // Publish ros message
  gpsfix_pub_.publish(output);
  } 


int main(int argc, char **argv)
{
  ros::init(argc, argv, "microstrain_gps_fix");
  ros::NodeHandle nh;
  ros::NodeHandle np("~");

  GpsFullToFix converter(nh, np);

  ros::spin();

  return 0;
}
